#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# ofertoj/forms.py

from models import Oferto
from profiles.models import Profile

from django.db import models

from turtle.forms import Turtle_Form

class OfertoCreateForm(Turtle_Form):
	
	class Meta:
		model = Oferto
		fields = (	"name",
					"description",
					"tags",
					"time",
					"requirements",
					"location",
					"image",
											
			)
	
					

	
        
class OfertoUpdateForm(OfertoCreateForm):
	class Meta:
		model = Oferto



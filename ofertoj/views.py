#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ofertoj.views

from .models import Oferto
from django.views.generic import CreateView, UpdateView, DetailView
from django.views.generic import ListView, TemplateView, RedirectView

from turtle.views import Turtle_CreateView,ActionMixin,NameSearchMixin
from braces.views import LoginRequiredMixin

from forms import OfertoCreateForm,OfertoUpdateForm

from models import Transaction
from profiles.models import Profile

from django.shortcuts import HttpResponse, redirect
from django.http import Http404


class OfertoCreateView(LoginRequiredMixin,Turtle_CreateView):
	model = Oferto
	action = "created"
	form_class = OfertoCreateForm
	

class OfertoDetailView(DetailView):
	model = Oferto
	template_name = "ofertoj/oferto_detail.html"

	def get_context_data(self,**kwargs):
		context = super(OfertoDetailView,self).get_context_data(**kwargs)
		return context


class OfertoUpdateView(LoginRequiredMixin,ActionMixin,UpdateView):
	model = Oferto
	action = "created"
	form_class = OfertoUpdateForm

	
class OfertoListView(ListView):
	model = Oferto
	paginate_by = 10
	template_name = "ofertoj/oferto_list.html"
	# context_object_name = "offertos"

	def get_context_data(self,**kwargs):
		context = super(OfertoListView,self).get_context_data(**kwargs)
		return context

	
class OfertoHome(ListView):
	model = Oferto
	paginate_by = 10
	template_name = "ofertoj/oferto_list.html"
	context_object_name = "ofertoj"

	def get_context_data(self,**kwargs):
		context = super(OfertoHome,self).get_context_data(**kwargs)
		return context


class TransactionView(TemplateView, LoginRequiredMixin):
	template_name = "ofertoj/offer_accepted.html"

	def get(self, request, *args, **kwargs):
		context = self.get_context_data(**kwargs)
		if self.request.GET.get("oferto_slug"):
			oferto = Oferto.objects.get(slug=self.request.GET.get("oferto_slug"))
			if oferto.valid:
				transaction = Transaction()
				transaction.create(
					creator=self.request.GET.get("creator"),
					amount=oferto.time,
					accepted_by=self.request.user.profile.slug,
					oferto_slug = oferto.slug,
				)
				acceptor = Profile.objects.get(user=self.request.user)
				if not acceptor.balance:
					acceptor.balance = 0
					acceptor.save()
				acceptor.balance = acceptor.balance - oferto.time
				acceptor.save()

				# credit the coins to the creator
				creator = Profile.objects.get(user=oferto.user)
				if not creator.balance:
					creator.balance = 0
					creator.save()
				creator.balance = creator.balance + oferto.time
				creator.save()
			else:
				return HttpResponse("This offer is currently valid")
		else:
			raise Http404

		return self.render_to_response(context)



class OfferSwitch(TemplateView):

	def get(self, request, *args, **kwargs):
		context = self.get_context_data(**kwargs)
		if self.request.GET.get("offer_slug"):
			oferto = Oferto.objects.get(slug=self.request.GET.get("offer_slug"))
			if self.request.GET.get("switch") == "on":
				oferto.valid = True
			else:
				oferto.valid = False
			oferto.save()

		return redirect("/oferto/" + oferto.slug)
		
class TurnTransactionView(TemplateView,LoginRequiredMixin):

	def get(self, request, *args, **kwargs):
		context = self.get_context_data(**kwargs)
		if self.request.GET.get("transaction_pk"):
			transaction = Transaction.objects.get(pk=request.GET.get("transaction_pk"))
			creator = Transaction.objects.get(creator="creator")
			accepted_by = Transaction.objects.get(accepted_by="accepted_by")
			if self.request.GET.get("Accept") == True:
				transaction.state = 'a'
				creator.balance = creator.balance - transaction.amount
				accepted_by.balance = accpeted_by.balance + transaction.amount
				
			elif self.request.GET.get("delivered") == True:
				transaction.state = 'd'
				
			elif self.request.GET.get("cancel") == True:
				transaction.state = 'c'
				accepted_by.balance = accpeted_by.balance - transaction.amount
				created_by.balance = created_by.balance + transaction.amount
				
			elif self.request.GET.get("refund") == True:
				transaction.state = 'r'
				accepted_by.balance = accpeted_by.balance - transaction.amount
				created_by.balance = created_by.balance + transaction.amount

		
		else:
				return HttpResponse("Transaction_ERROR")
		
		return redirect("/oferto/transaction_detail/" + transaction.pk)

			
class TransactionDetailView(DetailView):
	model = Transaction
	template_name = "ofertoj/transaction_detail.html"

	

class TransactionList(ListView):
	''' list the transactions
	'''
	model = Transaction
	paginate_by = 20
	template_name = "ofertoj/transaction_list.html"


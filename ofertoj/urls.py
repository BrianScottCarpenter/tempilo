#!/usr/bin/env python
# -*- coding: utf-8 -*-
# ofertoj/urls.py

from django.conf.urls import patterns, url
from views import *

urlpatterns = patterns("",
	url(
	regex=r"^createview/$",
	view=OfertoCreateView.as_view(),
	name="oferto_createview"
	),
	
	url(
	regex=r"^listview/$",
	view=OfertoListView.as_view(),
	name="oferto_listview"
	),

	url(
	regex=r"^accept/$",
	view = TransactionView.as_view(),
	name = "accept_offer"
	),

	url(
	regex=r"^offer_turn/$",
	view = OfferSwitch.as_view(),
	name = "offer_switch"
	),

	url(
	regex=r"^transaction_list/$",
	view = TransactionList.as_view(),
	name = "transaction_list"
	),
	
	url(
	regex=r"transaction_detail/(?P<pk>[-_\w]+)/$",
	view=TransactionDetailView.as_view(),
	name="transaction_detail"
	),
	
	url(
	regex=r"transaction_turn/$",
	view=TurnTransactionView.as_view(),
	name="transaction_turn"
	),
	
	url(
	regex=r"^(?P<slug>[-_\w]+)/$",
	view=OfertoDetailView.as_view(),
	name="oferto_detail"
	),
	
	url(
	regex=r"^(?P<slug>[-_\w]+)/updateview/$",
	view=OfertoUpdateView.as_view(),
	name="oferto_updateview"
	),
	
	url(
	regex=r"^$",
	view = OfertoHome.as_view(),
	name ="oferto_home"
	)
)

#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.db import models

from django.contrib.auth.models import User
from django_extensions.db.fields import AutoSlugField
from django.contrib import admin
from django.core.urlresolvers import reverse

from django.contrib import admin


import tagging
from tagging.models import Tag

from turtle.models import BaseInfo
from x.models import X
from profiles.models import Profile


class Oferto(models.Model):
	user = models.ForeignKey(User)
	name = models.CharField(max_length=150)
	description = models.TextField(max_length=3000)
	time = models.DecimalField(max_digits= 999,decimal_places =2,null= True)
	stelo = models.DecimalField(max_digits= 999,decimal_places =2,null= True)
	location = models.TextField(max_length=3000)
	slug = AutoSlugField(('slug'), max_length=128, unique=True, populate_from=('name',))
	tags = tagging.fields.TagField()
	image = models.ImageField(upload_to='Ofertoj',blank=True, null=True)
	requirements = models.TextField(max_length=550000,blank=True, null=True)
	valid = models.BooleanField(default=True)

	def get_absolute_url(self):
		return reverse('oferto_detail', kwargs={'slug': self.slug})
			
	def __unicode__(self):
		return self.name
		
	def get_tags(self):
		return Tag.objects.get_for_object(self) 

			
class Transaction(models.Model):
	creator = models.SlugField()
	amount = models.DecimalField(max_digits= 999,decimal_places =2,null= True,default=0)
	accepted_by = models.SlugField()
	oferto_slug = models.SlugField()
	STATUSES = (
        ('i', 'Initiated'),
        ('a', 'Accepted'),
        ('d', 'Delivered'),
        ('c', 'Cancelled'),
        ('r', 'Refunded'),
    )
	status = models.CharField(choices=STATUSES, default='i',max_length=10)


	def __unicode__(self):
		return self.id

	def create(self, **kwargs):
		new_transaction = Transaction(
				creator = kwargs['creator'],
				amount = kwargs['amount'],
				accepted_by = kwargs['accepted_by'],
				oferto_slug = kwargs['oferto_slug'],
				status = 'i'
			)
		new_transaction.save()
		return
		
	def get_absolute_url(self):
		return reverse('transaction_detail', kwargs={'id': self.id})
	


admin.site.register(Oferto)

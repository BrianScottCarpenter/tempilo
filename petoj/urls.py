#!/usr/bin/env python
# -*- coding: utf-8 -*-
# petoj/urls.py

from django.conf.urls import patterns, url
from views import *

urlpatterns = patterns("",
	url(
	regex=r"^createview/$",
	view=PetoCreateView.as_view(),
	name="peto_createview"
	),
	
	url(
	regex=r"^listview/$",
	view=PetoListView.as_view(),
	name="peto_listview"
	),
		
	url(
	regex=r"^(?P<slug>[-_\w]+)/$",
	view=PetoDetailView.as_view(),
	name="peto_detail"
	),
	
	
	url(
	regex=r"^(?P<slug>[-_\w]+)/updateview/$",
	view=PetoUpdateView.as_view(),
	name="peto_updateview"
	),
	
	url(
	regex=r"^$",
	view = PetoHome.as_view(),
	name ="peto_home"
	),
)



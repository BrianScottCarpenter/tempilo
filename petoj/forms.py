#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# petoj/forms.py

from models import Peto
from profiles.models import Profile

from django.db import models


from turtle.forms import Turtle_Form

class PetoCreateForm(Turtle_Form):
	
	class Meta:
		model = Peto
		fields = (	"name",
					"description",
					"tags",
					"time",
					"requirements",
					"location",
					"image",
											
			)
	
					

	
        
class PetoUpdateForm(PetoCreateForm):
	class Meta:
		model = Peto



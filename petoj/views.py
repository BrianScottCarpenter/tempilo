#!/usr/bin/env python
# -*- coding: utf-8 -*-
# petoj.views

from .models import Peto
from django.views.generic import CreateView, UpdateView, DetailView
from django.views.generic import ListView

from turtle.views import Turtle_CreateView,ActionMixin,NameSearchMixin
from braces.views import LoginRequiredMixin

from forms import PetoCreateForm,PetoUpdateForm


class PetoCreateView(LoginRequiredMixin,Turtle_CreateView):
	model = Peto
	action = "created"
	form_class = PetoCreateForm
	
	

class PetoDetailView(DetailView):
	model = Peto
	

class PetoUpdateView(LoginRequiredMixin,ActionMixin,UpdateView):
	model = Peto
	action = "created"
	form_class = PetoUpdateForm
	
class PetoListView(ListView):
	model = Peto
	paginate_by = 10
	template_name = "petoj/peto_list.html"

	def get_context_data(self,**kwargs):
		context = super(PetoListView,self).get_context_data(**kwargs)
		return context

	
class PetoHome(ListView):
	model = Peto
	paginate_by = 10
	template_name = "petoj/peto_list.html"

	def get_context_data(self,**kwargs):
		context = super(PetoHome,self).get_context_data(**kwargs)
	

from django.contrib.auth.models import User
from django import template

from .models import Blog

register = template.Library()

@register.filter
def is_blogger(user):
    if not isinstance(user, User):
        raise template.TemplateSyntaxError("is_blogger filter requires a single argument - a User instance" % token.contents.split()[0])

    return bool(Blog.objects.filter(user=user))

def top_tags(tags, _count=0):
    try:
        count = int(_count)
    except ValueError:
        return

    s_tags = sorted(tags, key=lambda x: x.count, reverse=True)
    return s_tags[:count] if count else s_tags
register.filter(top_tags)

class AuthorEntriesNode(template.Node):
    def __init__(self, entries, var_name):
        self.entries = template.Variable(entries)
        self.var_name = var_name

    def render(self, context):
        try:
            entry_list = self.entries.resolve(context)
        except template.VariableDoesNotExist:
            return ''

        author_entry_dict = {}
        for entry in entry_list:
            if entry.authors.all()[0] in author_entry_dict: continue
            author_entry_dict[entry.authors.all()[0]] = entry

        context[self.var_name] = sorted(author_entry_dict.values(), key=lambda x: x.creation_date, reverse=True)[:20]
        return ''

@register.tag
def get_author_latest_entry(parser, token):
    """ get_author_latest_entry entry_list as author_entries """
    error = False
    try:
        # split_contents() knows not to split quoted strings.
        tag_name, entries, _, var_name = token.split_contents()
    except ValueError:
        error = True
    if _ != "as": error = True
    if error:
        raise template.TemplateSyntaxError("%r tag is of format: 'get_author_entries entry_list as author_entries'" % token.contents.split()[0])
    return AuthorEntriesNode(entries, var_name)


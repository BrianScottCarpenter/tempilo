#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  blogs/forms.py
#  
#  Copyright 2013 Brian Scott Carpenter <talisman@talisman-Pangolin-Performance>

from django.forms import ModelForm
from models import Blog

class BlogEntryForm(ModelForm):
	class Meta:
		model = Blog
		fields = ['user','location']

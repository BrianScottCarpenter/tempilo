from django.conf.urls.defaults import *

urlpatterns = patterns('',
    (r'^del/(?P<object_id>[0-9]+)/$', 'blogs.views.delete_entry',),
    (r'^(?P<username>\w+)/(?P<object_id>[0-9]+)/$', 'blogs.views.view_entry'),
    (r'^create/$', 'blogs.views.create_blog'),
    (r'^(?P<object_id>[0-9]+)/edit/$', 'blogs.views.update_blog'),
    (r'^(?P<object_id>[0-9]+)/delete/$', 'blogs.views.delete_entry'),
    # zinnia
  #  url(r'^$', 'blogs.views.view_blogs', {'template_name': 'blogs.zinnia/entry_archive.html'}),
    url(r'^', include('zinnia.urls')),
 #   (r'^(?P<location>[-\w]+)/$', 'blogs.views.view_blog'),
 
	(r'^static/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.STATIC_ROOT}),
)

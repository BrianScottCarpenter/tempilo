from django.contrib import admin
from models import Blog


class BlogAdmin(admin.ModelAdmin):
    list_select_related = True
    list_display = ('__str__', 'user', 'location',)
    search_fields = ('user__username', 'user__first_name', 'user__email',)
    save_on_top = True

admin.site.register(Blog, BlogAdmin)

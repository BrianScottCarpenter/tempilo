from django.contrib.auth.models import User
from django.db import models


class Blog(models.Model):
    # zinnia doesn't really support multiple blogs, just multiple users
    # we kind of fake multiple blog support here
    # this model allows us to give non-admin users blog editing rights
    # and also to set up user-editable urls for individual blogs
    user = models.ForeignKey(User, unique=True, related_name='blog_user_blog', help_text="The blog is editable in this user's profile page.")
    location = models.CharField(max_length=75, verbose_name='Address of blog', help_text='Your blog will be available at http://www.mywebsite.com/blogs/ADDRESS/ - Please enter only lowercase letters, numbers and hyphens')

    def __str__(self):
        return "%s" % self.user.username

	def get_absolute_url(self):
		return reverse("blog_detail", kwargs={"slug": self.slug}) 



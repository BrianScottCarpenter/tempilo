from copy import copy
from datetime import datetime, timedelta

from models import Blog
from forms import BlogEntryForm

from zinnia.models import Entry as BlogEntry
from zinnia.views.authors import AuthorDetail

# from zinnia.views.decorators import update_queryset

from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.contrib.sites.models import Site
from django.http import Http404
from django.shortcuts import get_object_or_404, render_to_response, redirect
from django.template import RequestContext
from django.template.defaultfilters import slugify
from django.views.decorators.csrf import csrf_protect
#from django.views.generic.list_detail import object_list

@csrf_protect
@login_required
def update_blog(request, object_id=None):
    # only people allowed edit blogs are staff or users that staff
    # have granted blogs to
    if not request.user.is_staff and not Blog.objects.filter(user=request.user):
        raise Http404
    # TODO - decide if we want non-staff bloggers to be able to edit
    # each other's blogs or not (currently they can)
    entry = get_object_or_404(BlogEntry, id=object_id)

    if request.POST:
        form = BlogEntryForm(request.POST, request.FILES, instance=entry)
        if form.is_valid():
            form.save()
            return redirect(entry)
    else:
        form = BlogEntryForm(instance=entry)

    return render_to_response('blogs/blog_form.html',
            {'form': form, 'entry': entry},
            context_instance=RequestContext(request))


@csrf_protect
@login_required
def create_blog(request):
    if not request.user.is_staff and not Blog.objects.filter(user=request.user):
        raise Http404

    if request.POST:
        post = copy(request.POST)
        form = BlogEntryForm(post, request.FILES)
        if form.is_valid():
            # for simplicity our form omits a lot of fields that
            # zinnia requires. Here we fill in the missing bits
            entry = form.save()
            entry.status = 2
            entry.last_update = datetime.now()
            entry.template = "zinnia/entry_detail.html"
            entry.start_publication = datetime.now()
            entry.creation_date = datetime.now()
            entry.end_publication = datetime(2042, 3, 15)
            entry.slug = slugify(post['title'])
            entry.authors = (request.user,)
            entry.sites = Site.objects.all()
            entry.save()
            return redirect(entry)
    else:
        form = BlogEntryForm()

    return render_to_response('blogs/blog_form.html',
            {'form': form},
            context_instance=RequestContext(request))

@login_required
def delete_entry(request, object_id):
    entry = get_object_or_404(BlogEntry, id=object_id)
    if request.user not in entry.authors.all(): raise Http404
    entry.delete()
    return redirect('/blogs/authors/%s/' % request.user, context_instance=RequestContext(request)) # TODO: replace with view

@csrf_protect
def view_entry(request, username, object_id):
    user = get_object_or_404(User, username__exact=username)
    entry = get_object_or_404(BlogEntry, id__exact=object_id)
    variables = RequestContext(request, {
      'username': user.username,
      'profile': user.get_profile(),
      'entry': entry,
    })
    return render_to_response('blogs/entry_detail.html',
            variables,context_instance=RequestContext(request))

@csrf_protect
def view_blog(request, location, public_profile_field=None):
    user = get_object_or_404(Blog, location=location).user
    return AuthorDetail(request, user)

cutoff = datetime.now() - timedelta(days=300)
def qset():
    return BlogEntry.objects.filter(status=2).filter(creation_date__gte=cutoff)
    
# view_blogs = update_queryset(object_list, qset)

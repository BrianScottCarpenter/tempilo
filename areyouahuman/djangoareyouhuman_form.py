from django.conf import settings

import ayah
ayah.configure(
    settings.AREYOUHUMAN_PUBLISHER_KEY,
    settings.AREYOUHUMAN_SCORING_KEY,
)

from django.forms import Field, ValidationError, Form
from django.forms.widgets import Widget


#Create a wdiget that renders the AreYouHuman html
class AreYouHumanWidget(Widget):

    def render(self, name, value, attrs=None):
        return ayah.get_publisher_html()


#AreYouHuman uses a test field (session_secret) for validation
class AreYouHumanField(Field):

    #Use the AreYouHuman HTML
    widget = AreYouHumanWidget

    #Validate agianst AreYouHuman
    def validate(self, value):
        if not ayah.score_result(value):
            raise ValidationError("You may not be human.")


# Because AreYouHuman does currently
# allow you to change the field name
# (session secret) we have to create a form
class AreYouHumanForm(Form):
    session_secret = AreYouHumanField(label="Are You Human")

#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  views.py
from django.conf import settings
from django.template import loader
from django.template import Context
from django.http import HttpResponseServerError
from django.shortcuts import render_to_response,get_object_or_404
from django.template import RequestContext

from django.shortcuts import render_to_response
from django.http import HttpResponseRedirect, HttpResponse

from django.views.generic import TemplateView



def home (request):
        """ This is the Home view"""
        return render_to_response(('index.html'),context_instance=RequestContext(request))
        
def auth_logout(request):
        """ logout the user"""
        return render_to_response(('registration/logout.html'),context_instance=RequestContext(request))

def auth_login(request):
        """ login the user"""
        return render_to_response(('registration/login.html'),context_instance=RequestContext(request))

def auth_password_change(request):
        """change the user's password"""
        return render_to_response(('registration/password_change_form.html'),context_instance=RequestContext(request))        

def server_error(request, template_name='500.html'):
    """
    500 error handler.
    Templates: `500.html`
    Context:
    STATIC_URL
      Path of static media (e.g. "media.example.org")
    """
    t = loader.get_template(template_name)
    return HttpResponseServerError(
        t.render(Context({'STATIC_URL': settings.STATIC_URL})))



import socket
import sys
import os.path
import os
from os.path import join, dirname, normpath
import dj_database_url
import psycopg2

LOCAL_PATH = normpath(join(dirname(__file__), '..'))

if socket.gethostname() == 'talisman-Pangolin-Performance': #Brian's Development Laptop
	DEBUG = TEMPLATE_DEBUG = True
        DATABASES = {
		'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2', # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': 'mydb3',             # Or path to database file if using sqlite3.
        'USER': 'dev1',                      # Not used with sqlite3.
        'PASSWORD': '12981210',                  # Not used with sqlite3.
        'HOST': 'localhost',                      # Set to empty string for localhost. Not used with sqlite3.
        'PORT': '',                      # Set to empty string for default. Not used with sqlite3.
    }
}
        #DATABASES['default'] =  dj_database_url.config()
        SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')
        ALLOWED_HOSTS = ['*']
        BASE_DIR = os.path.dirname(os.path.abspath(__file__))
        MEDIA_URL = '/media/'
        STATIC_ROOT = 'staticfiles'
        STATIC_URL = '/static/'
        PROJECT_PATH = os.path.dirname(os.path.abspath(__file__))
        STATICFILES_DIRS = (
				os.path.join(PROJECT_PATH, 'static'),
		)
        PROJECT_URL='http://0.0.0.0:5000/'
        PROJECT_DIR = os.path.dirname(__file__) # this is not Django setting.
        # here() gives us file paths from the root of the system to the directory
        # holding the current file
        here = lambda * x: os.path.join(os.path.abspath(os.path.dirname(__file__)), *x)
        PROJECT_ROOT = here("..")
        # root() gives us file paths from the root of the system to whatever
        # folder(s) we pass it starting at the parent directory of the current file.
        root = lambda * x: os.path.join(os.path.abspath(PROJECT_ROOT), *x)
        TEMPLATE_DIRS = (
			root('templates'),)
			
			
        EMAIL_HOST = 'mailtrap.io'
        EMAIL_HOST_USER = 'heroku-7c1e6a2b336a9a08'
        EMAIL_HOST_PASSWORD = 'd9548d77d1eaf965'
        EMAIL_PORT = '2525'
        EMAIL_USE_TLS = False
	
else:
        DEBUG = TEMPLATE_DEBUG = False
        DATABASES = {
		'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2', # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': 'dak3bepd5m9h8j',             # Or path to database file if using sqlite3.
        'USER': 'lbhppzicdylnbq',                      # Not used with sqlite3.
        'PASSWORD': '5oAf-k8hCnV9pL8dqcdbuRFNUr',                  # Not used with sqlite3.
        'HOST': 'ec2-54-197-238-8.compute-1.amazonaws.com',                      # Set to empty string for localhost. Not used with sqlite3.
        'PORT': '5432',                      # Set to empty string for default. Not used with sqlite3.
    }
}
        DATABASES['default'] =  dj_database_url.config()
        SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')
        ALLOWED_HOSTS = ['*']
        BASE_DIR = os.path.dirname(os.path.abspath(__file__))
        MEDIA_URL = '/media/'
        STATIC_ROOT = 'staticfiles'
        STATIC_URL = '/static/'
        PROJECT_PATH = os.path.dirname(os.path.abspath(__file__))
        STATICFILES_DIRS = (
				os.path.join(PROJECT_PATH, 'static'),
		)
        PROJECT_URL='tempilo.herokuapp.com'
        PROJECT_DIR = os.path.dirname(__file__) # this is not Django setting.
        # here() gives us file paths from the root of the system to the directory
        # holding the current file
        here = lambda * x: os.path.join(os.path.abspath(os.path.dirname(__file__)), *x)
        PROJECT_ROOT = here("..")
        # root() gives us file paths from the root of the system to whatever
        # folder(s) we pass it starting at the parent directory of the current file.
        root = lambda * x: os.path.join(os.path.abspath(PROJECT_ROOT), *x)
        TEMPLATE_DIRS = (
			root('templates'),)

        EMAIL_HOST_USER = 'app22917296@heroku.com'
        EMAIL_HOST= 'smtp.sendgrid.net'
        EMAIL_PORT = 587
        EMAIL_USE_TLS = True
        EMAIL_HOST_PASSWORD = '3xwjwiss'
		
if socket.gethostname() == "virendra":
    TEMPLATE_DIRS = "/home/virendra/proj/tempilo/templates"
    SQL_LITE_NAME = "db16.db"
    TEMPLATE_DEBUG = DEBUG = True
    STATICFILES_DIRS = (
        "/home/virendra/proj/tempilo/static",
    )
    PROJECT_URL = 'http://localhost:8000/'


ADMINS = (
     ('Brian Carpenter', 'brianscottcarpenter@gmail.com'),
)

MANAGERS = ADMINS



# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# In a Windows environment this must be set to your system time zone.
TIME_ZONE = 'America/Chicago'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'en-us'

SITE_ID = 2

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale.
USE_L10N = True

# If you set this to False, Django will not use timezone-aware datetimes.
USE_TZ = True

# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/home/media/media.lawrence.com/media/"
MEDIA_ROOT = ''

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash.
# Examples: "http://media.lawrence.com/media/", "http://example.com/media/"

# URL prefix for static files.
# Example: "http://media.lawrence.com/static/"
STATIC_URL = '/static/'
                        

# Additional locations of static files



# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    #'django.contrib.staticfiles.finders.DefaultStorageFinder',
)



# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
    'django.template.loaders.eggs.Loader',
)

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    # Uncomment the next line for simple clickjacking protection:
     'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'tempilo.urls'

# Python dotted path to the WSGI application used by Django's runserver.
WSGI_APPLICATION = 'tempilo.wsgi.application'


INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    # Uncomment the next line to enable the admin:
     'django.contrib.admin',
    # Uncomment the next line to enable admin documentation:
	'django.contrib.admindocs',
	'django.contrib.comments',
	'django.contrib.sitemaps',
     
    'zinnia',
    'zinnia_gallery',
    'tagging',
	'mptt',
	'south',
	'registration',
	'blogs',
	'turtle',
	
	'ofertoj',
	'petoj',
	'x',
	'profiles',
	'postman',

	'gunicorn',
	'psycopg2',
   # 'django_comments',

    'robots'


)

# A sample logging configuration. The only tangible logging
# performed by this configuration is to send an email to
# the site admins on every HTTP 500 error when DEBUG=False.
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        }
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
    }
}

TEMPLATE_CONTEXT_PROCESSORS = (
  'django.contrib.auth.context_processors.auth',
  'django.core.context_processors.i18n',
  'django.core.context_processors.debug',
  'django.core.context_processors.request',
  'django.core.context_processors.media',
  'django.core.context_processors.static',
  'django.contrib.messages.context_processors.messages',
  'postman.context_processors.inbox',
  'zinnia.context_processors.version',) # Optional


# Registration
ACCOUNT_ACTIVATION_DAYS = 7


#1 postman
POSTMAN_AUTO_MODERATE_AS = True


# for profiles module to work with django

AUTH_PROFILE_MODULE ="profiles.Profile"

## Login Redirect URL fix
LOGIN_REDIRECT_URL = '/'

AREYOUHUMAN_PUBLISHER_KEY = '87e31f12336132203bdde2da8c93269fccdd5f52'
AREYOUHUMAN_SCORING_KEY = '85a4f7bc3e475623f825262a1c9f6c408e368dcf'

# Make this unique, and don't share it with anybody.
SECRET_KEY = 'hb7^j0!*s_1yv0g)($pizcej0so7w24afp!o!=peemjk)qm!ij'

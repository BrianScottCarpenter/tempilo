#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# profiles/forms.py

from models import Profile

from turtle.forms import Turtle_Form

class ProfileCreateForm(Turtle_Form):

	class Meta:
		model = Profile
		fields = (	
					
					"contact_name",
					"bio",
					"phone",
					"address",
					"city",
					"state",
					"code",
					"country",
					"image",
					"tags",

					)

        
class ProfileUpdateForm(ProfileCreateForm):
	pass





# profiles.models
from django.contrib.auth.models import User
from django.contrib import admin
from django.core.urlresolvers import reverse

from django.contrib.auth.models import User
from django.db.models import signals
from django.dispatch import Signal

from django.conf import settings

from django.db import models
from turtle.models import BaseInfo

from django_extensions.db.fields import AutoSlugField

import tagging
from tagging.models import Tag

class Profile(models.Model):
	bio = models.TextField(max_length=15000000)
	user = models.OneToOneField(User)
	
	contact_name = models.CharField(max_length=100)
	phone = models.CharField(max_length=20)
	address = models.CharField(max_length=3000)
	city = models.CharField(max_length=3000)
	state = models.CharField(max_length=3000)
	code = models.IntegerField(null=True)
	country = models.CharField(max_length=3000)

	image = models.ImageField(upload_to='photos/%Y/%m/%d',blank=True)

	slug = AutoSlugField(('slug'), max_length=128, unique=True, populate_from=('user',))
	tags = tagging.fields.TagField()
	balance = models.DecimalField(max_digits= 999,decimal_places =2,null= True,default=0)
		
	def get_tags(self):
		return Tag.objects.get_for_object(self) 



	def get_absolute_url(self):
		return reverse('profile_detail', kwargs={'slug': self.slug})
	

### This Signal creates the Profile, when the user account is activated
def user_post_save(sender, instance, signal, *args, **kwargs):
    # Creates user profile
    profile, new = Profile.objects.get_or_create(user=instance)


signals.post_save.connect(user_post_save, sender=User)


admin.site.register(Profile)

# tempilo.UserProfiles
from models import Profile
from django.db import models.AbstractBaseUser


class MyUser(AbstractBaseUser):
    identifier = models.CharField(max_length=40, unique=True)
    USERNAME_FIELD = 'identifier'
    
    profile = OneToOneField(Profile,primary_key=True)

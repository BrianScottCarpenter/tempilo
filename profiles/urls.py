#!/usr/bin/env python
# -*- coding: utf-8 -*-
# profiles/urls.py

from django.conf.urls import patterns, url
from .views import *

urlpatterns = patterns("",

	url(
	regex=r"^createview/$",
	view=ProfileCreateView.as_view(),
	name="profile_createview"
	),
	
	url(
	regex=r"^listview/$",
	view=ProfileListView.as_view(),
	name="profile_listview"
	),

	url(
	regex=r"(?P<slug>[-_\w]+)/Ofertolist/$",
	view=ViewProfileListKajOferto.as_view(),
	name="profile_oferto_list"
	),
	
		url(
	regex=r"(?P<slug>[-_\w]+)/Petolist/$",
	view=ViewProfileListKajPeto.as_view(),
	name="profile_peto_list"
	),
		
	url(
	regex=r"^(?P<slug>[-_\w]+)/$",
	view=ProfileDetailView.as_view(),
	name="profile_detail"
	),

	
	url(
	regex=r"^(?P<slug>[-_\w]+)/updateview/$",
	view=ProfileUpdateView.as_view(),
	name="profile_updateview"
	),
	
	url(
	regex=r"^$",
	view = ProfileHome.as_view(),
	name ="profile_home"
	),
)




# Create your views here.

#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Profiles.views

from .models import Profile
from ofertoj.models import Oferto
from petoj.models import Peto

from django.views.generic import CreateView, UpdateView, DetailView, TemplateView
from django.views.generic import ListView

from turtle.views import Turtle_CreateView,ActionMixin,NameSearchMixin
from braces.views import LoginRequiredMixin
from zinnia.views.channels import EntryChannel

from forms import ProfileCreateForm,ProfileUpdateForm

from django.shortcuts import get_object_or_404

from django.contrib.auth.models import User

from django.db import models


class ProfileCreateView(LoginRequiredMixin,Turtle_CreateView):
	model = Profile
	action = "created"
	form_class = ProfileCreateForm
	

class ProfileDetailView(DetailView):
	model = Profile
	template_name = 'profiles/profile_detail.html'
	

class ProfileUpdateView(LoginRequiredMixin,ActionMixin,UpdateView):
	model = Profile
	action = "created"
	form_class = ProfileUpdateForm

	
class ProfileListView(ListView):
	model = Profile
	paginate_by = 10


	
class ProfileHome(ListView):
	context_object_name = 'home_list'    
	template_name = 'profiles/profile_home.html'
	queryset = Profile.objects.all()

	def get_context_data(self, **kwargs):
		context = super(ProfileHome, self).get_context_data(**kwargs)
		context['profile'] = Profile.objects.all()
		return context
		
		
class ViewProfileListKajOferto(ListView):
	template_name = 'profiles/profile_kaj_oferto.html'
	model = Oferto
	context_object_name = 'Ofertoj'
	
	def get_queryset(self):
		self.slug = self.kwargs['slug']
		# filter the offers created by the user in the url
		queryset = Oferto.objects.filter(user__profile__slug=self.slug)
		return queryset

	def get_context_data(self, **kwargs):
		context = super(ViewProfileListKajOferto, self).get_context_data(**kwargs)
		context['slug'] = Profile.objects.get(slug=self.slug)
		context['profile'] = Profile.objects.get(slug=self.slug)
		return context


class ViewProfileListKajPeto(ListView):
	template_name = 'profiles/profile_kaj_peto.html'
	model = Peto
	context_object_name = 'Petoj'
	
	def get_queryset(self):
		self.slug = self.kwargs['slug']
		queryset = Peto.objects.filter(user=self.request.user)
		return queryset

	def get_context_data(self, **kwargs):
		context = super(ViewProfileListKajPeto, self).get_context_data(**kwargs)
		context['slug'] = Profile.objects.get(slug=self.slug)
		context['profile'] = Profile.objects.get(slug=self.slug)
		return context

# zinnia_gallery.models

from django.db import models
from django.contrib import admin


from zinnia.models_bases.entry import AbstractEntry
from ofertoj.models import Oferto

class Gallery(AbstractEntry):
	Oferto = models.ForeignKey(Oferto)

	
	def __unicode__(self):
		return u'Gallery %s' % self.name
		
	class Meta(AbstractEntry.Meta):
		abstract = True


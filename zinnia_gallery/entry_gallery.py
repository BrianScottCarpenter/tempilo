#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  zinnia_gallery.entry_gallery.py
#  
#  Copyright 2013 Brian Scott Carpenter <talisman@Pangolin>
from django.db import models
from zinnia_gallery.models import Gallery
from zinnia.models import EntryAbstractClass

from ofertoj.models import Oferto

class EntryGallery(EntryAbstractClass):
    gallery = models.ForeignKey(Gallery)
	oferto = models.ForeignKey(Oferto)

    def __unicode__(self):
        return u'EntryGallery %s' % self.title

    class Meta(EntryAbstractClass.Meta):
        abstract = True


#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  turtle.forms.py
#  
#  Copyright 2013 Brian Scott Carpenter <talisman@talisman-Pangolin-Performance>

from django import forms
from django.conf import settings


from django.forms import Field, ValidationError, Form
from django.forms.widgets import Widget


#Create a wdiget that renders the AreYouHuman html
class AreYouHumanWidget(Widget):

    def render(self, name, value, attrs=None):
        return ayah.get_publisher_html()


#AreYouHuman uses a test field (session_secret) for validation
class AreYouHumanField(Field):

    #Use the AreYouHuman HTML
    widget = AreYouHumanWidget

    #Validate agianst AreYouHuman
    def validate(self, value):
        if not ayah.score_result(value):
            raise ValidationError("You may not be human.")


# Because AreYouHuman does currently
# allow you to change the field name
# (session secret) we have to create a form
class AreYouHumanForm(Form):
    session_secret = AreYouHumanField(label="Are You Human")


class Turtle_Form(forms.ModelForm):
	''' This is code to make sure the User automatically saves the
	user to the database in context.
	'''
	def save(self, *args, **kwargs):
		kwargs['commit']=False
		obj = super(Turtle_Form, self).save(*args, **kwargs)
		if self.request:
			obj.user = self.request.user
			obj.save()
		return obj #<--- Return saved object to caller.
	
	def __init__(self, *args, **kwargs):
		self.request = kwargs.pop('request', None)
		return super(Turtle_Form, self).__init__(*args, **kwargs)
		
class Turtle_BaseInfoForm(Turtle_Form):
	class meta:
		fields = (	
					"contact_name",
					"email",
					"phone",
					"address",
					"city",
					"state",
					"code",
					"country",
					"image",
					"tags",

					)

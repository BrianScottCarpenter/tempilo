#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Create your views here.
# turtle.views

from django.views.generic import CreateView, UpdateView, DetailView
from django.contrib import messages

class Turtle_CreateView(CreateView):

    def get_form_kwargs(self):
        # Ensure the current `request` is provided to subclassform.
        kwargs = super(Turtle_CreateView, self).get_form_kwargs()
        kwargs.update({ 'request': self.request })
        return kwargs
        
class ActionMixin(object):
	@property
	def action(self):
		msg = "{0} is missing action.".format(self.__class__)
		raise NotImplementedError(msg)
	
	def form_valid(self, form):
		msg = "{0}!".format(self.action)
		messages.info(self.request, msg)
		return super(ActionMixin, self).form_valid(form)
		
	def form_invalid(self, form):
		# Do custom logic here
		return super(ActionMixin, self).form_invalid(form)
		
		
class NameSearchMixin(object):
	def get_queryset(self):
		# Fetch the queryset from the parent get_queryset
		queryset = super(NameSearchMixin,self).get_queryset()
		
		#Get the q parameter
		q = self.request.GET.get("q")
		if q:
				# Return a filtered Queryset
				return queryset.filter(name__icontains=q)
				
		#Return the base queryset
			
		return queryset
			

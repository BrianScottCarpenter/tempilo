#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.db import models
from django.contrib.auth.models import User

from django_extensions.db.fields import AutoSlugField

import tagging
from tagging.models import Tag


# Create your models here.
# turtle.models

class BaseInfo(models.Model):
	contact_name = models.CharField(max_length=100)
	email = models.EmailField(max_length=75)
	phone = models.CharField(max_length=20)
	address = models.CharField(max_length=3000)
	city = models.CharField(max_length=3000)
	state = models.CharField(max_length=3000)
	code = models.IntegerField()
	country = models.CharField(max_length=3000)

	image = models.ImageField(upload_to='photos/%Y/%m/%d',blank=True)

	slug = AutoSlugField(('slug'), max_length=128, unique=True, populate_from=('name',))
	tags = tagging.fields.TagField()

	def __unicode__(self):
		return self.name
		
	def get_tags(self):
		return Tag.objects.get_for_object(self) 


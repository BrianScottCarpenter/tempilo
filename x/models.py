# x
from django.db import models
from django.contrib import admin


# Create your models here.
class X (models.Model):
	name = models.CharField(max_length=150)
	description = models.TextField(max_length=3000)
	x = models.TextField(max_length=30000000)

admin.site.register(X)
